<title>Show</title>

<?php 

require_once '../../../../src/bitm/seip131181/mobile/Semister.php';

use SemisterApp\bitm\seip131181\mobile\Semister ;

$objShow = new Semister;

$SingleData = $objShow -> prepare($_GET) -> show();

?>

</head>
<body>

<div class="container">
  <h2>All Data In List View</h2>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
        <th>Unique Id</th>
	<tr>
        <td><?php echo $SingleData['name']; ?></td>
        <td><?php echo $SingleData['name']; ?></td>
        <td><?php echo $SingleData['semister']; ?></td>
        <td><?php echo $SingleData['offer']; ?></td>
        <td><?php echo $SingleData['cost']; ?></td>
        <td><?php echo $SingleData['waiver']; ?></td>
        <td><?php echo $SingleData['total']; ?></td>
        <td><?php echo $SingleData['unique_id']; ?></td>
	</tr>
</table>

<p><a href="index.php">View the list</a></p>