<title>Update</title>

<?php 

require_once '../../../../src/bitm/seip131181/mobile/Semister.php';

use SemisterApp\bitm\seip131181\mobile\Semister ;

$objUpdate = new Semister;

$objUpdate -> prepare($_POST) -> update();
