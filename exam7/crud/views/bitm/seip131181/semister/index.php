<?php

require_once '../../../../src/bitm/seip131181/semister/Semister.php';

use SemisterApp\bitm\seip131181\semister\Semister;

$objIndex = new Semister();

$AllData = $objIndex -> prepare($_POST) -> index();

?>

<div class="container">
  <h2>List of Semister Informataion</h2>            
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Waiver</th>
        <th>Total</th>
        <th colspan="3">Action</th>
      </tr>
    </thead>
    <tbody>
           <?php
           $serial = 1;
           foreach ($AllData as $SingleData){
               ?>
           
      <tr>
        <td><?php echo $serial++ ?></td>
        <td><?php echo $SingleData['name'] ?></td>
        <td><?php echo $SingleData['semister'] ?></td>
        <td><?php echo $SingleData['offer'] ?></td>
        <td><?php echo $SingleData['cost'] ?></td>
        <td><?php echo $SingleData['waiver'] ?></td>
        <td><?php echo $SingleData['total'] ?></td>
        <td><a href="show.php?id=<?php echo $SingleData['unique_id'] ?>">View</a></td>
        <td><a href="edit.php?id=<?php echo $SingleData['unique_id'] ?>">Edit</a></td>
        <td><a href="delete.php?id=<?php echo $SingleData['unique_id'] ?>">Delete</a></td>
      </tr>
      <?php 
      
      
           }
      ?>
    </tbody>
  </table>
  <a href="creat.php">Creat new Item</a>
</div>

       </body>
</html>
