<?php
include_once '../../../../vendor/autoload.php';

 use MobileApp\Bitm\Seip10\Mobile\Mobile;
 $obj = new Mobile();
$Alldata = $obj->index();
?>
<a href="../../../../index.php">List of Project</a>
<a href="create.php">Add New Model</a>
<?php

if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
<head>
    <title>Index | Data</title>
</head>
<body>
<table border="1">
    <tr>
        <th>SL</th>
        <th>Title</th>
        <th>Unique Id</th>
        <th>Laptop</th>
        <th colspan="3">Action</th>
    </tr>
    <?php

    $serial = 1;
    if (isset($Alldata) && !empty($Alldata)) {

        foreach ($Alldata as $Singledata) {
            ?>

            <tr>
                <td><?php echo $serial++ ?></td>
                <td><?php echo $Singledata['Mobile'] ?></td>
                <td><?php echo $Singledata['laptop'] ?></td>
                <td><?php echo $Singledata['unique_id'] ?></td>
                <td><a href="show.php?id=<?php echo $Singledata['unique_id'] ?>">View</a></td>
                <td><a href="edit.php?id=<?php echo $Singledata['unique_id'] ?>">Edit</a></td>
                <td><a href="delete.php?id=<?php echo $Singledata['unique_id'] ?>">Delete</a></td>

            </tr>
        <?php }
    } else {
        ?>
        <tr>
            <td colspan="3">
                No available data
            </td>
        </tr>
    <?php } ?>
</table>
       <a href="pdf.php">Download as PDF</a>
       <a href="xl.php">Download as Excel</a>
</body>
</html>
