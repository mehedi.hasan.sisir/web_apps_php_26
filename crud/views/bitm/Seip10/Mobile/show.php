<?php
include_once '../../../../vendor/autoload.php';

 use MobileApp\Bitm\Seip10\Mobile\Mobile;

$object = new Mobile();
//print_r($_GET);

$onedata = $object->prepare($_GET)->show();

if(isset($onedata) && !empty($onedata) ){
?>
<a href="index.php">Back to list</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Unique Id</th>
    </tr>
    <tr>
        <td><?php echo $onedata['id'] ?></td>
        <td><?php echo $onedata['Mobile'] ?></td>
        <td><?php echo $onedata['unique_id'] ?></td>
    </tr>
</table>
<?php

}
else{
    $_SESSION['Err_Show'] = "Not found. Something going wrong. <a href='index.php'>Go Back</a>";
    header('location:errors.php');
}