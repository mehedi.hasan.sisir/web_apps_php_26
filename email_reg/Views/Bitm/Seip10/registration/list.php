<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;

$objlist = new Registration();

$data = $objlist ->alluser();

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {
    
    
    if(!empty($data['birthdate'])){
    $bday = unserialize($data['birthdate']);}
    if(!empty($data['interest'])){
    $interest = unserialize($data['interest']);}
    if(!empty($data['languages'])){
    $language = unserialize($data['languages']);}
    if(!empty($data['languages'])){
    $address = unserialize($data['address']);}
    
    
    if( $_SESSION['user']['is_admin'] ==1){
        $i = 1; $serial = 0;

        ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>List</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    
    <?php // echo "<pre>"; print_r($data);?>

<div class="container" style="margin-top:5%;">
    <div class="row">
        
       <?php $objlist->warningMsg('listSuccess'); ?>
            
           <table class="table table-bordered">

                 <h2 class="center-block" align="center">List of All Users</h2>
                <tr class="info">
                  <th>ID</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Email</th>
                  <th>Admin</th>
                  <th>Education</th>
                  <th>Marritial State</th>
                  <th>Job_status</th>
                  <th>Nationality</th>
                  <th>Blood</th>
                  <th>Mobile</th>
                  <th>Skill_arel</th>
                  <th>Interest</th>
                  <th>Address</th>
                  <th>Religion</th>
                  <th>Language</th>
                  <th>Birthday</th>
                  <th>Created</th>
                  <th>Modified</th>
                  <th>Deleted</th>
                  <th colspan="3" style="text-align: center;">Action</th>
                </tr>
            </div>
               <div class="panel-body">
                   
                       <tbody>
                            <?php
                            foreach($data as $singleData){
                                if($singleData['is_deleted'] == 1){
                                    continue;
                                }
                                
                                ?>
                          <tr class="<?php      if($i %2 == 0){
                                                echo "success";}else{
                                                    echo "warning";
                                                }
                                                $i++;  ?>">
                                <td><?php echo $serial++;?></td>
                                <td><?php echo $singleData['username']; ?></td>
                                <td><?php echo $singleData['password']; ?></td>
                                <td><?php echo $singleData['email']; ?></td>
                                <td><?php echo $singleData['is_admin']; ?></td>
                                <td><?php echo $singleData['education']; ?></td>
                                <td><?php echo $singleData['marritial_stat']; ?></td>
                                <td><?php echo $singleData['job_status']; ?></td>
                                <td><?php echo $singleData['Nationality']; ?></td>
                                <td><?php echo $singleData['blood']; ?></td>
                                <td><?php echo $singleData['mobile']; ?></td>
                                <td><?php echo $singleData['skill_arel']; ?></td>
                                <td><?php if(!empty($singleData['interest'])){
                                        $inter = unserialize($singleData['interest']);
                                    foreach($inter as $value){
                                        echo $value." ";
                                        }
                                    }?></td>
                                
                                <td><?php if(!empty($singleData['address'])){
                                        $add = unserialize($singleData['address']);
                                    foreach($add as $value){
                                        echo $value." ";
                                        }
                                    }?>
                                </td>
                                <td><?php echo $singleData['religion']; ?></td>
                                <td><?php if(!empty($singleData['languages'])){
                                        $lang = unserialize($singleData['languages']);
                                    foreach($lang as $value){
                                        echo $value." ";
                                        }
                                    }?></td>
                                <td><?php if(!empty($singleData['birthdate'])){
                                        $birth = unserialize($singleData['birthdate']);
                                    foreach($birth as $value){
                                        echo $value." ";
                                        }
                                    }?></td>
                                <td><?php echo $singleData['created']; ?></td>
                                <td><?php echo $singleData['modified']; ?></td>
                                <td><?php echo $singleData['deleted']; ?></td>

                                <td><a href="index.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">View</td>
                                <td><a href="edit.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Edit</td>
                                <td><a href="trash.php?id=<?php echo $singleData['unique_id']; ?>" class="btn btn-primary">Delete</td>

                          </tr>
                          <?php 
                            }
                            ?>
                    </tbody>
                  </table>
            
    <a href="trashlist.php">Want to see trash list</a>

<?php include 'footermenu.php'; ?>

<?php
    } else{
        $_SESSION['errorMsg'] = "This page is only for ";
         header("location:error.php");
    }

        

    
}else{
    
    $_SESSION['errorMsg'] = "Sorry ! You don't have the permission to access";
    header("location:error.php");
}
  
        
?>