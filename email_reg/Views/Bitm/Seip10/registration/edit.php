<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';
use RegApp\Bitm\Seip10\registration\Registration;


$objEdit = new Registration();



if(isset($_GET['id'])){
    $userData = $objEdit ->prepare($_GET)-> singleUser();
}else{
    
    $_GET['id'] = $_SESSION['user']['unique_id'];
    $userData = $objEdit ->prepare($_GET)-> singleUser();
}



if(!empty($userData['birthdate'])){
$bday = unserialize($userData['birthdate']);}

if(!empty($userData['interest'])){
$interest = unserialize($userData['interest']);}

if(!empty($userData['languages'])){
$language = unserialize($userData['languages']);}


$address = unserialize($userData['address']);

if(!empty($_SESSION['user']) && isset($_SESSION['user'])) {

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <style>.autowidth {display:inline-block !important;width:auto !important;</style>
</head>
 
 <body>
      <div class="container" style="margin-top:5%;margin-bottom:5%">
         <div class="row">
                 <!-- left column -->
    <div class="update_pic col-md-5">
        <div class="text-center">
            <img src="../img/user1.jpeg" class="avatar img-circle img-thumbnail" alt="avatar">
            <h6>Upload a different photo...</h6>
            <input type="file" class="text-center center-block well well-sm">
        </div>
    </div><!-- update_pic -->
                 
    <!-- edit form column -->
    <div class="update_info col-md-7">
        <a class="btn btn-default" href="index.php?id=<?php echo $userData['unique_id'];?>">Back to profile</a>
      <h2>Update Personal info</h2><?php $objEdit ->warningMsg('status') ?>
      <br>
     
            <form class="form-horizontal" role="form" method="POST" action="update.php">

              <label>First name:</label>
              <input class="form-control" name="full_name" value="<?php echo $userData['full_name']; ?>" type="text"><br>

              <label>Father name:</label>
              <input class="form-control" name = "father_name" value="<?php echo $userData['father_name']; ?>" type="text"><br>

              <label>Mother name:</label>
              <input class="form-control" name = "mother_name" value="<?php echo $userData['mother_name']; ?>" type="text"><br>

              <label>Update your birthday :</label>
                 <div>
                     <select name="day">
                          <?php
                              for($i=1; $i <=31; $i++){
                                  echo "<option value=".$i." ".($bday['day']==$i ? 'selected':'').">".$i."</option>";
                              }
                              
                          ?>
                      </select>
                      <!--input month-->
                      <select name="month"> 
                          <option value="January" <?php if(!empty($bday) && $bday['month']=='January'){echo'selected';}?>>January</option>
                          <option value="February"<?php if(!empty($bday) && $bday['month']=='February'){echo'selected';}?>>February</option>
                          <option value="March"<?php if(!empty($bday) && $bday['month']=='March'){echo'selected';}?>>March</option>
                          <option value="April"<?php if(!empty($bday) && $bday['month']=='April'){echo'selected';}?>>April</option>
                          <option value="May"<?php if(!empty($bday) && $bday['month']=='May'){echo'selected';}?>`>May</option>
                          <option value="June"<?php if(!empty($bday) && $bday['month']=='June'){echo'selected';}?>>June</option>
                          <option value="July"<?php if(!empty($bday) && $bday['month']=='July'){echo'selected';}?>>July</option>
                          <option value="September"<?php if(!empty($bday) && $bday['month']=='September'){echo'selected';}?>>September</option>
                          <option value="October"<?php if(!empty($bday) && $bday['month']=='October'){echo'selected';}?>>October</option>
                          <option value="November"<?php if(!empty($bday) && $bday['month']=='November'){echo'selected';}?>>November</option>
                          <option value="December"<?php if(!empty($bday) && $bday['month']=='December'){echo'selected';}?>>December</option>    
                      </select>
                      <!--input year-->
                      <select name="year">
                          <?php
                          for($i =1976;$i <= 2014; $i++){

                              echo "<option value=".$i." ".($bday['year']==$i ? 'selected':'').">".$i."</option>";
                          }
                          ?>
                      </select>
                 </div><br>

              <label>Your Gender : </label>
              <div>
                  <input type="radio" name="gender" value="male" <?php echo ($userData['gender'] == 'male'?'checked':''); ?>>  Male
                  <input type="radio" name="gender" value="female" <?php echo ($userData['gender'] == 'female'?'checked':''); ?>>  Female
              </div><br>

              <label>Occupation:</label>
              <input class="form-control" name = "occupation" value="<?php echo $userData['occupation']; ?>" type="text"><br>
              
              <label>Education:</label>
              <input class="form-control" name = "education" value="<?php echo $userData['education']; ?>" type="text"><br>
              
              <label>Religion:</label>
              <input class="form-control" name = "religion" value="<?php echo $userData['religion']; ?>" type="text"><br>
              
              <label>Marritial Status:</label>
              <input class="form-control" name = "marritial_stat" value="<?php echo $userData['marritial_stat']; ?>" type="text"><br>
              
              <label>Job Status:</label>
              <input class="form-control" name = "job_status" value="<?php echo $userData['job_status']; ?>" type="text"><br>
              
              <label>Nationality :</label>
              <input class="form-control" name = "Nationality" value="<?php echo $userData['Nationality']; ?>" type="text"><br>
              
              <label>Interest :</label><br>
              <input type="checkbox" name = "interest[]" value="Reading" 
                  <?php 
                  if(!empty($interest)){
                      foreach($interest as $value)
                          {if ($value=='Reading')
                              {echo 'checked';}
                              }
                                } ?>> Reading Book
              <input type="checkbox" name = "interest[]" value="Coding" <?php 
                  if(!empty($interest)){
                      foreach($interest as $value)
                          {if ($value=='Coding')
                              {echo 'checked';}
                              }
                                } ?>> Coding & music
              <input type="checkbox" name = "interest[]" value="Cycling" <?php 
                  if(!empty($interest)){
                      foreach($interest as $value)
                          {if ($value=='Cycling')
                              {echo 'checked';}
                              }
                                } ?>> Cycling  <br><br>
              
              <label>Bio:</label>
              <textarea class="form-control" name="bio"><?php echo $userData['bio']; ?></textarea>
              
              <label>Mobile Number :</label>
              <input class="form-control" name = "mobile" value="<?php echo $userData['mobile']; ?>" type="number"><br>
              
              <label>National ID Number :</label>
              <input class="form-control" name = "nid" value="<?php echo $userData['nid']; ?>" type="number"><br>
              
              <label>PassPort Number :</label>
              <input class="form-control" name = "passpoet" value="<?php echo $userData['passpoet']; ?>" type="number"><br>
              
              <label>Fax Number :</label>
              <input class="form-control" name = "fax" value="<?php echo $userData['fax']; ?>" type="number"><br>
              
              <label>Skills Area :</label>
              <input class="form-control" name = "skill_arel" value="<?php echo $userData['skill_arel']; ?>" type="text"><br>
              
              <label>Languages :</label>
              <input type="checkbox" name = "languages[]" value="Bangla" <?php 
                  if(!empty($language)){
                      foreach($language as $value)
                          {if ($value=='Bangla')
                              {echo 'checked';}
                                }
                                  } ?>> Bangla  
              <input type="checkbox" name = "languages[]" value="English" <?php 
                  if(!empty($language)){
                      foreach($language as $value)
                          {if ($value=='English')
                              {echo 'checked';}
                                }
                                  } ?>>  English  
              <input type="checkbox" name = "languages[]" value="Hindi" <?php 
                  if(!empty($language)){
                      foreach($language as $value)
                          {if ($value=='Hindi')
                              {echo 'checked';}
                                }
                                  } ?>>  Hindi<br><br>
              
              <label>Blood Group:</label>
              <select name="blood[]"> 
                  <option value="A+" <?php if($userData['blood']=='A+'){echo 'selected';} ?>>A+</option>
                  <option value="B+" <?php if($userData['blood']=='B+'){echo 'selected';} ?>>B+</option>
                  <option value="AB+" <?php if($userData['blood']=='AB+'){echo 'selected';} ?>>AB+</option>
                  <option value="O+" <?php if($userData['blood']=='O+'){echo 'selected';} ?>>O+</option>
                  <option value="A-" <?php if($userData['blood']=='A-'){echo 'selected';} ?>>A-</option>
                  <option value="B-" <?php if($userData['blood']=='B-'){echo 'selected';} ?>>B-</option>
                  <option value="AB-" <?php if($userData['blood']=='AB-'){echo 'selected';} ?>>AB-</option>
                  <option value="O-" <?php if($userData['blood']=='O-'){echo 'selected';} ?>>O-</option>
              </select><br>
              
              <label>Height :</label>
              <input class="form-control" name = "height" value="<?php echo $userData['height']; ?>" type="text"><br>
              
              <label>Address :</label>
              <input class="form-control" name = "add1" value="<?php echo $address['add1']; ?>" placeholder="Address" type="text"><br>
              
              <input class="form-control autowidth" name = "street" value="<?php echo $address['street']; ?>" placeholder="Area/Police Station" type="text">
              <input class="form-control autowidth" name = "city" value="<?php echo $address['city']; ?>" placeholder="City/Division" type="text">
              <input class="form-control autowidth" name = "zip" value="<?php echo $address['zip']; ?>" placeholder="ZIP Code" type="number"><br>
              
              
              <label>Other :</label>
              <input class="form-control" name = "other" value="<?php echo $userData['other']; ?>" type="text"><br><br>
              
              <input class="btn btn-primary" value="Save Changes" type="submit">

        </form>
    </div> <!-- update_info -->
            
<?php include 'footermenu.php'; ?>

<?php
    
}else{
    
    $_SESSION['errorMsg'] = "Sorry ! You don't have the permission to access";
    header("location:error.php");
}
  
        
?>