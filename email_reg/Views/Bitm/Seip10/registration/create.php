<?php

require_once '../../../../Src/Bitm/Seip10/registration/Registration.php';

use RegApp\Bitm\Seip10\registration\Registration;


$objCreat = new Registration();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Registration</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    <div class="container" style="margin-top:5%;">
    <div class="row">
        <?php $objCreat ->warningMsg('storeMsg');?>
        <fieldset>

            
        <legend class="text-center"><h2>Please Register Here</h2></legend>

            <form class="form-horizontal center-block" method="POST" action="store.php">
                
                
                <!--*************************************************-->
                <!--C***********Code for input Username **************-->
                <!--*************************************************-->
                                
                
              <div class="form-group"><!--code for input Username -->
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-6">
                            <input type="text" name="uname" class="form-control" value="<?php $objCreat ->warningMsg('show_username'); ?>" placeholder="Enter Username">
                            
                        </div><?php $objCreat ->warningMsg('unique_user'); $objCreat ->warningMsg('user_char'); $objCreat ->warningMsg('user_empty'); ?>
              </div><!--form-group of end code for input Username -->
                
              
              
                <!--*************************************************-->
                <!--C***********Code for input Password **************-->
                <!--*************************************************-->
                
                
                                
                <div class="form-group"><!--code for input Username -->
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-6">
                          <input type="password" class="form-control" name="passwrd" value="" placeholder="Enter password ">
                        </div><?php $objCreat ->warningMsg('password_char');$objCreat ->warningMsg('password_empty'); ?>
                         
              </div><!--form-group of end code for input Username -->
              
              
              
                <!--*************************************************-->
                <!--C**********Code for Confirm Password ************-->
                <!--*************************************************-->
                
                
              
                <div class="form-group"><!--code for input Username -->
                        <label class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-6">
                          <input type="password" class="form-control" name="passwrd2" id="pwd" placeholder="Enter confirm password">
                        </div><?php $objCreat ->warningMsg('confirm_password_match');$objCreat ->warningMsg('confirm_password_empty'); ?>
                                                 
              </div><!--form-group of end code for input Username -->
              
              
              
                <!--*************************************************-->
                <!--C************Code for Input Email ***************-->
                <!--*************************************************-->
                
                
              
                <div class="form-group"><!--code for input Username -->
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                          <input type="email" class="form-control" name="email" id="email" value="<?php $objCreat ->warningMsg('show_email'); ?>" placeholder="Enter email">
                        </div><?php $objCreat ->warningMsg('unique_email'); $objCreat ->warningMsg('invalid_format'); $objCreat ->warningMsg('email_empty'); ?>
                        
              </div><!--form-group of end code for input Username -->
              
              
                
              
                <!--*************************************************-->
                <!--C************Button for Submit Form ***************-->
                <!--*************************************************-->
                
              
              <button type="submit" class="btn btn-primary col-md-8">Submit</button>

            </form>
        </fieldset>
        <br>
        <a href="login.php" class="btn btn-warning btn-lg btn-block">Click Here for Login if you Already have a account</a>
        
            <?php include 'footermenu.php'; ?>