-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 09, 2016 at 02:46 পূর্বাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise`
--

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

CREATE TABLE `profile` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `mother_name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `religion` varchar(255) NOT NULL,
  `marritial_stat` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `job_status` varchar(255) NOT NULL,
  `Nationality` varchar(255) NOT NULL,
  `interest` varchar(255) NOT NULL,
  `bio` text NOT NULL,
  `nid` int(11) NOT NULL,
  `passpoet` int(255) NOT NULL,
  `skill_arel` varchar(255) NOT NULL,
  `languages` varchar(255) NOT NULL,
  `profile_pic` longblob NOT NULL,
  `blood` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `user_id`, `full_name`, `father_name`, `mother_name`, `gender`, `birthdate`, `occupation`, `education`, `religion`, `marritial_stat`, `mobile`, `job_status`, `Nationality`, `interest`, `bio`, `nid`, `passpoet`, `skill_arel`, `languages`, `profile_pic`, `blood`, `height`, `fax`, `address`, `other`, `created`, `modified`, `deleted`) VALUES
(1, 60, 'Arina', 'Sabbir', 'Poly', '', 'a:3:{s:3:"day";s:1:"1";s:5:"month";s:7:"January";s:4:"year";s:4:"1976";}', 'MTO', 'BBA', 'Islam', 'unmarried', '17115601998', 'GP Tel', 'Bangladeshi', '', 'Hi, I am full of confidence', 2147483647, 2147483647, 'Review Writing', '', '', 'A+', '5'' 4', '17115601998', 'a:4:{s:4:"add1";s:8:"Mirupr 6";s:6:"street";s:7:"Pallabi";s:4:"city";s:5:"Dhaka";s:3:"zip";s:4:"1216";}', 'nai', '2016-08-11 00:00:00', '2016-08-09 01:45:42', '2016-08-25 00:00:00'),
(2, 66, 'Arina', 'Kupila', 'sultaqna', '', 'a:3:{s:3:"day";s:1:"1";s:5:"month";s:7:"January";s:4:"year";s:4:"1976";}', 'MTO', 'BBA', 'Islam', 'unmarried', '17115601998', 'GP Tel', 'Bangladeshi', '', 'Hi, I am full of confidence', 2147483647, 2147483647, 'Review Writing', '', '', 'A+', '5'' 4', '17115601998', '', 'nai', '0000-00-00 00:00:00', '2016-08-08 01:18:12', '0000-00-00 00:00:00'),
(3, 67, 'Dashing Dalim', 'Abdul Alim', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', 'A+', '', '', '', '', '2016-08-06 09:48:03', '2016-08-08 12:30:40', '0000-00-00 00:00:00'),
(4, 68, 'Dashing Dalim', 'Abdul Alim', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', 'A+', '', '', '', '', '2016-08-06 10:25:25', '2016-08-08 12:30:40', '0000-00-00 00:00:00'),
(5, 69, 'Abu Bakar', 'Abdul Alim', 'Ruksana Begum', 'male', 'a:3:{s:3:"day";s:2:"23";s:5:"month";s:8:"February";s:4:"year";s:4:"1988";}', 'Student', 'CSE', 'Islam', 'unmarried', '0175601998', 'SEIP', 'Bangladeshi', 'a:2:{i:0;s:7:"reading";i:1;s:6:"coding";}', 'Hi I am confident', 175601998, 175601998, '175601998', 'a:2:{i:0;s:6:"Bangla";i:1;s:7:"English";}', '', 'O+', '5'' 4', '175601998', 'a:4:{s:4:"add1";s:8:"house: 6";s:6:"street";s:8:"mirpur10";s:4:"city";s:5:"Dhaka";s:3:"zip";s:4:"1216";}', 'Nai kisu', '2016-08-08 08:28:38', '2016-08-08 03:53:07', '0000-00-00 00:00:00'),
(6, 71, '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', '', '', '', '', '', '', '', '2016-08-08 10:29:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `profile`
--
ALTER TABLE `profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
