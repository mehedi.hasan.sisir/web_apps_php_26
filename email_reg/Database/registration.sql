-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 09, 2016 at 02:46 পূর্বাহ্ণ
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exercise`
--

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verified_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `deleted` datetime NOT NULL,
  `is_deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`id`, `unique_id`, `verified_id`, `username`, `password`, `email`, `is_admin`, `is_active`, `created`, `deleted`, `is_deleted`) VALUES
(60, '57a0c08aabdcb', '57a0c08aabd90', 'mehedi123', 'qwerqwer', 'ratan@gmail.com', 1, 1, '2016-08-02 05:47:22', '0000-00-00 00:00:00', 0),
(66, '57a63ea15f6c0', '57a63ea15f680', 'checkfiled334', 'asdfasdf', 'ddeir@cddd.cdom', 0, 1, '2016-08-06 09:46:41', '0000-00-00 00:00:00', 0),
(67, '57a63ef3113f0', '57a63ef3113b6', 'sseeckfiled', 'asdfasdf', 'eddeir@cddd.cdom', 0, 1, '2016-08-06 09:48:03', '0000-00-00 00:00:00', 0),
(68, '57a647b5b09ca', '57a647b5b0990', 'asdfkjjh', 'asdfasdf', 'dseg@dfff.cds', 0, 1, '2016-08-06 10:25:25', '0000-00-00 00:00:00', 0),
(69, '57a82696454ed', '57a82696454b3', 'Bakar', 'asdfasdf', 'bakar@mehedi.com', 0, 1, '2016-08-08 08:28:38', '0000-00-00 00:00:00', 0),
(70, '57a8ea5beee7c', '57a8ea5beee38', 'Shuman', 'asdfasdf', 'shuman@seip.com', 0, 1, '2016-08-08 10:23:55', '0000-00-00 00:00:00', 0),
(71, '57a8eb94d0368', '57a8eb94d0327', 'Aslam', 'asdfasdf', 'aslam@mahbub.com', 0, 1, '2016-08-08 10:29:08', '0000-00-00 00:00:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `registration`
--
ALTER TABLE `registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
